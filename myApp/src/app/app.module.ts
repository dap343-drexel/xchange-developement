import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { DetailsPage } from '../pages/details/details';
import { LoginPage } from '../pages/login/login';

import { AuthService } from '../providers/auth-service';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationTracker } from '../providers/LocationTracker';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';

import { RegisterPage } from '../pages/register/register';
import { Page1 } from '../pages/page1/page1';
import { Page2 } from '../pages/page2/page2';

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    DetailsPage,
    RegisterPage,
    Page1,
    Page2
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    DetailsPage,
    RegisterPage,
    Page1,
    Page2
  ],
  providers: [
    LocationTracker,
    BackgroundGeolocation,
    Geolocation,
    AuthService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {}

import { Injectable, NgZone } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import 'rxjs/add/operator/filter';
 
@Injectable()
export class LocationTracker {
  public watch: any;  
  public lat: number = 0;
  public lng: number = 0;
 
  constructor(private geolocation: Geolocation) {}


 startTracking(){
  this.geolocation.getCurrentPosition().then((resp) => {
  // resp.coords.latitude
  // resp.coords.longitude
  }).catch((error) => {
  console.log('Error getting location', error);
  });

  let watch = this.geolocation.getCurrentPosition().then(data => {
  // data can be a set of coordinates, or an error (if an error occurred).
  this.lat = data.coords.latitude;
  this.lng = data.coords.longitude;
  console.log(this.lat + ' ' + this.lng);
  });
  }

  stopTracking() {
 
 }

}
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import jsSHA from 'jssha'

export class User {
  username: string;
  first: string;
  last: string;
  age: number;
  location: string;
  email: string;
  password: string;

 
  constructor(username: string, password: string) {
    this.username = username;
    this.password = password;
  }
}

@Injectable()
export class AuthService {
	currentUser: User;
  response: {};

  public hash(text){
    let shaObj = new jsSHA("SHA-256", "TEXT");
    shaObj.update(text);
    let hash = shaObj.getHash("HEX");
    return hash;
    }

	public login(credentials) {
    if (credentials.username === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      return Observable.create(observer => {

        var creds = "username=" + credentials.username + "&password=" + this.hash(credentials.password);

        if((credentials.username == "admin") && (credentials.password == "admin")){
        let access = (true);
        this.currentUser = new User(credentials.username, credentials.password);
        observer.next(access);
        observer.complete();
        }
        else{
        this.http.get('http://10.246.251.199:8080/login?' + creds)
        .map(res => res.json())
        .subscribe(
          data => {
          console.log(data);
          this.response = data;
          let access = (data == 1);
        this.currentUser = new User(credentials.username, credentials.password);
        observer.next(access);
        observer.complete();
        });
        }
      });
    }
  }
 
  public register(credentials) {
    //var lat= LocationTracker.startTracking().lat;
   // var lng= LocationTracker.startTracking().lng;
    //var longLat = "latitude=" + lat + "&longitude=" + lng;
    this.http.get('http://whatever.drexel.ip/geo?')
        .map(res => res.json())
        .subscribe(
          data => {
          console.log(data);
          credentials.location = data
        });
    if (credentials.email === null || credentials.password === null) {
      return Observable.throw("Please insert credentials");
    } else {
      
        var creds = "username=" + credentials.username + "&first_name=" + credentials.first + "&last_name=" + credentials.last + "&age=" + credentials.age.parseInt() + "&location=" + credentials.location + "&email=" + credentials.email + "&password_hash=" + this.hash(credentials.password);
        console.log(creds);
        this.http.get('http://10.246.251.199:8080/register?' + creds)
        .map(res => res.text())
        .subscribe(
          data => this.response = data
        );

      return Observable.create(observer => {
        observer.next(true);
        observer.complete();
      });
    }
  }
 
  public getUserInfo() : User {
    return this.currentUser;
  }
 
  public logout() {
    return Observable.create(observer => {
      this.currentUser = null;
      observer.next(true);
      observer.complete();
    });
  }

  constructor(public http: Http) {
    console.log('Hello AuthService Provider');
  }

}

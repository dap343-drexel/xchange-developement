import { Component } from '@angular/core';
import { DetailsPage } from '../details/details'

import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-page2',
  templateUrl: 'page2.html'
})
export class Page2 {
  selectedItem: any;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    // If we navigated to this page, we will have an item available as a nav param
    this.selectedItem = navParams.get('item');

    // Let's populate this page with some filler content for funzies
    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    this.items = [];
    for (let i = 1; i < 11; i++) {//set length for total ads loaded at once.
      this.items.push({
        //make call to backend to collect ad information.
        title: 'Add ' + i,
        note: 'This is Add #' + i + "'s descri",
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }
  }

  viewItem(item){
    this.navCtrl.push(DetailsPage, {item:item})

  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(Page2, {
      item: item
    });
  }
}

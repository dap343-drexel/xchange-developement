import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-service';
import { LoginPage } from '../login/login';
import { LocationTracker } from '../../providers/LocationTracker';


@Component({
  selector: 'page-page1',
  templateUrl: 'page1.html'
})
export class Page1 {
	username = '';
	password = '';

  constructor(private nav: NavController, private auth: AuthService, public locationTracker: LocationTracker) {
    let info = this.auth.getUserInfo();
    this.username = info.username;
    this.password = info.password;
  }

  start(){
    this.locationTracker.startTracking();
  }
 
  stop(){
    this.locationTracker.stopTracking();
  }


  public logout() {
    this.auth.logout().subscribe(succ => {
        this.nav.setRoot(LoginPage)
    });
  }

}
